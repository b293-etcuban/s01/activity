package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//this file serves as our index/entry point

//@SpringBoot Application this is called as "Annotation" Mark
//represented by the @ symbol
// this is a shortcut or method we manually create or describe our purpose or configure a framework
//Annotations are used to provide supplemental information about the program
//these are used to manage and configure the behavior of the framework.
//spring Boot scans for classes in its class path upon running and assigns behavior on them based on their annotations.

//to specify the main class of the SpringBoot application
@SpringBootApplication

//we added the restController
//tells the springBoot that this will handle endpoints for web requests
@RestController
//to handle endpoints and methods

public class Wdc044Application {
//	This method starts the whole SpringBoot Framework
	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

//	how to run our application?
//	./mvnw spring-boot:run

//	Mapping HTTP Get Request
	@GetMapping("/hello")

//	@RequestParam is used to extract query parameters, form parameters, and even files from teh request
//	/name = Cardo; Hello Cardo
//	name = World, Hello World
	// To append the URL with a name parameter we do the following:
	//http:localhost:8080/hello?name=john
//	"?" means the start of the parameters followed by the "key=value" pair

//	Access query parameter
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "World") String greet){
		return String.format("Good Evening, %s! Welcome to Batch 293!", greet);
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "User") String user){
		return String.format("hi %s!", user);
	}

	@GetMapping("/nameAge")
	public String nameAge(
			@RequestParam(value = "name", defaultValue = "User") String name,
			@RequestParam(value = "age", defaultValue = "0") int age) {
				return "Hello "+  name + "! Your age is " + age + ".";
	}


}
